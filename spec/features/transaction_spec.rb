require 'rails_helper'

RSpec.feature "Transactions", type: :feature do
  scenario 'deposit' do
    create(:entity, name: 'Mirzalazuardi', type: 'User')
    visit root_path

    click_on 'Deposit'
    fill_in 'transaction_amount', with: '10'
    click_on 'Process'

    expect(page).to have_text("Mirzalazuardi").and have_text("10.0")
  end

  scenario 'deposit added' do
    u = create(:entity, name: 'Mirzalazuardi', type: 'User')
    create(:transaction, amount: 10, source_wallet_id: u.wallet.id)
    visit root_path

    click_on 'Deposit'
    fill_in 'transaction_amount', with: '5'
    click_on 'Process'

    expect(page).to have_text("Mirzalazuardi").and have_text("15.0")
  end

  scenario 'withdraw' do
    u = create(:entity, name: 'Mirzalazuardi', type: 'User')
    create(:transaction, amount: 10, source_wallet_id: u.wallet.id)
    visit root_path

    click_on 'Withdraw'
    fill_in 'transaction_amount', with: '7'
    click_on 'Process'

    expect(page).to have_text("Mirzalazuardi").and have_text("3.0")
  end

  scenario 'transfer' do
    u1 = create(:entity, name: 'Mirzalazuardi', type: 'User')
    u2 = create(:entity, name: 'Alpha', type: 'Team')
    create(:transaction, amount: 10, source_wallet_id: u1.wallet.id)
    visit root_path

    click_on 'Transfer', match: :first
    fill_in 'transaction_amount', with: '7'
    select 'Alpha', from: 'transaction_target_wallet_id'
    click_on 'Process'

    expect(page).to have_text("7.0").and have_text("3.0")
  end
end
