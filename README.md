# Ewallet Test

## Installation

please edit `config/database.yml` with your db credential

run `bin/dev`

## Test

run `rspec`

## description

There are two main resource: Entity and Transaction

on Entity list display all User, Team & Stock, with its current balance.

on Transaction list display balance sheet of all transactions.

I also prevent conflict for multiple edit in transaction using pessimistic locking.
