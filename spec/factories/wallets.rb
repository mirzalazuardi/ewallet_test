FactoryBot.define do
  factory :wallet do
    entity { nil }
    uuid { SecureRandom.uuid.gsub('-', '') }
  end
end

# == Schema Information
#
# Table name: wallets
#
#  id         :bigint           not null, primary key
#  uuid       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  entity_id  :bigint           not null
#
# Indexes
#
#  index_wallets_on_entity_id  (entity_id)
#  index_wallets_on_uuid       (uuid) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (entity_id => entities.id)
#
