require 'rails_helper'

RSpec.describe Transaction, type: :model do
  let(:entity1) { create :entity }
  let(:wallet1) { create(:wallet, entity: entity1) }
  let(:entity2) { create :entity }
  let(:wallet2) { create(:wallet, entity: entity2) }

  context 'validations' do
    describe 'presence' do
      it { should validate_presence_of(:amount) }
    end
    describe 'numericality' do
      it { should validate_numericality_of(:amount).is_greater_than(0.0) }
    end
  end

  context 'class methods' do
    describe '.deposit' do
      it 'return 10 after deposit(10)' do
        described_class.deposit(wallet1, 10)

        expect(entity1.balance.to_s).to eq '10.0'
      end
    end
    describe '.transfer' do
      it 'return 7 after transfer 3 to wallet2' do
        described_class.deposit(wallet1, 10)
        described_class.transfer(wallet1, wallet2, 3)

        expect(entity1.balance.to_s).to eq '7.0'
      end
      it 'return 3 after transfered 3 from wallet1' do
        described_class.deposit(wallet1, 10)
        described_class.transfer(wallet1, wallet2, 3)

        expect(entity2.balance.to_s).to eq '3.0'
      end
    end
    describe '.withdraw' do
      it 'return 2 after withdraw 8' do
        described_class.deposit(wallet1, 10)
        described_class.withdraw(wallet1, 8)

        expect(entity1.balance.to_s).to eq '2.0'
      end
    end
  end
end

# == Schema Information
#
# Table name: transactions
#
#  id               :bigint           not null, primary key
#  amount           :decimal(9, 2)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  source_wallet_id :integer
#  target_wallet_id :integer
#
