module TransactionHelper
  def transaction_row(transaction)
    attrs = [transaction.amount, transaction.source_wallet,
             transaction.target_wallet, transaction.created_at]

    send("transaction_debit_#{transaction.act_type}_type", *attrs)
      .concat(send("transaction_credit_#{transaction.act_type}_type", *attrs))
  end

  def transaction_debit_deposit_type(amount, source, _target, created_at)
    src = source&.entity&.name || '(deleted entity)'
    src += source&.entity&.type ? '(' + source&.entity&.type + ')' : ''
    tag.tr
       .concat(tag.td(amount))
       .concat(tag.td)
       .concat(tag.td("Deposit to #{src}"))
       .concat(tag.td(created_at.to_fs(:short)))
       .html_safe
  end

  def transaction_credit_deposit_type(amount, _source, _target, created_at)
    tag.tr
       .concat(tag.td)
       .concat(tag.td(amount))
       .concat(tag.td('Cash inflow'))
       .concat(tag.td(created_at.to_fs(:short)))
       .html_safe
  end

  def transaction_debit_withdraw_type(amount, _source, _target, created_at)
    tag.tr
       .concat(tag.td(amount))
       .concat(tag.td)
       .concat(tag.td('Cash outflow'))
       .concat(tag.td(created_at.to_fs(:short)))
       .html_safe
  end

  def transaction_credit_withdraw_type(amount, _source, target, created_at)
    trg = target&.entity&.name || '(deleted entity)'
    trg += target&.entity&.type ? "(#{target&.entity&.type})" : ''

    tag.tr
       .concat(tag.td)
       .concat(tag.td(amount))
       .concat(tag.td("Withdraw from #{trg}"))
       .concat(tag.td(created_at.to_fs(:short)))
       .html_safe
  end

  def transaction_debit_transfer_type(amount, _source, target, created_at)
    trg = target&.entity&.name || '(deleted entity)'
    trg += target&.entity&.type ? "(#{target&.entity&.type})" : ''
    tag.tr
       .concat(tag.td(amount))
       .concat(tag.td)
       .concat(tag.td("Deduct from #{trg}"))
       .concat(tag.td(created_at.to_fs(:short)))
       .html_safe
  end

  def transaction_credit_transfer_type(amount, source, _target, created_at)
    src = source&.entity&.name || '(deleted entity)'
    src += source&.entity&.type ? '(' + source&.entity&.type + ')' : ''
    tag.tr
       .concat(tag.td)
       .concat(tag.td(amount))
       .concat(tag.td("Transfer to #{src}"))
       .concat(tag.td(created_at.to_fs(:short)))
       .html_safe
  end
end
