class ChangeAmountFromTransaction < ActiveRecord::Migration[7.0]
  def change
    change_column :transactions, :amount, :decimal, precision: 9, scale: 2
  end
end
