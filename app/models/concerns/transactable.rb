module Transactable
  extend ActiveSupport::Concern

  class_methods do
    def transfer(source_wallet, target_wallet, amount)
      raise NotEnoughMoneyError if amount > source_wallet.balance

      transaction do
        source_wallet.lock!
        target_wallet.lock!
        attrs = { source_wallet_id: target_wallet.id, target_wallet_id: source_wallet.id, amount: amount }
        create!(**attrs)
      end
    end

    def withdraw(target_wallet, amount)
      raise NotEnoughMoneyError if amount > target_wallet.balance

      transaction do
        target_wallet.lock!
        attrs = { source_wallet_id: nil, target_wallet_id: target_wallet.id, amount: amount }
        create!(**attrs)
      end
    end

    def deposit(source_wallet, amount)
      raise InvalidAmountError if amount < 0

      transaction do
        source_wallet.lock!
        attrs = { source_wallet_id: source_wallet.id, target_wallet_id: nil, amount: amount }
        create!(**attrs)
      end
    end

    def transaction_attrs_strategy(params)
      attrs = [params[:amount].to_f]
      wallet = ->(id) { Wallet.find(id) }
      attrs.unshift(wallet.call(params[:source_wallet_id])) if params[:act] == 'deposit'
      attrs.unshift(wallet.call(params[:target_wallet_id])) if params[:act] == 'withdraw'
      attrs.unshift(wallet.call(params[:target_wallet_id])) if params[:act] == 'transfer'
      attrs.unshift(wallet.call(params[:source_wallet_id])) if params[:act] == 'transfer'

      attrs
    end
  end
end
