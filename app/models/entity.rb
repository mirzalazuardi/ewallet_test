class Entity < ApplicationRecord
  has_one :wallet, dependent: :destroy

  validates_inclusion_of :type, in: %w[User Team Stock]
  validates :name, :type, presence: true

  after_create :create_wallet

  def create_wallet
    create_wallet!
  end

  delegate :balance, to: :wallet, allow_nil: true
end

# == Schema Information
#
# Table name: entities
#
#  id         :bigint           not null, primary key
#  name       :string
#  type       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
