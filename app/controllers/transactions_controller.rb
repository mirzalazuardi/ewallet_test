class TransactionsController < ApplicationController
  before_action :init_transaction, only: %i[deposit transfer withdraw]
  before_action :set_wallet, only: %i[withdraw transfer]

  respond_to :html

  def index
    @transactions = Transaction.all

    respond_with(@transactions)
  end

  def deposit; end

  def transfer
    @wallets = Wallet.includes(:entity)
                     .where.not(entities: { id: params[:entity_id] })
                     .pluck('entities.name, wallets.id')
  end

  def withdraw; end

  def compute
    raise InvalidTransactionActError unless %w[deposit transfer withdraw].include?(params[:transaction][:act])

    attrs = Transaction.transaction_attrs_strategy(params[:transaction])
    Transaction.send(params[:transaction][:act], *attrs)
    redirect_to entities_path
  end

  private

  def init_transaction
    @transaction = Transaction.new
  end

  def set_wallet
    @wallet = wallet
  end

  def transaction_params
    params.require(:transaction)
          .permit(:amount, :source_wallet_id, :target_wallet_id, :act)
  end
end
