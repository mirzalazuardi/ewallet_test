class Transaction < ApplicationRecord
  include Transactable

  validates :amount, presence: true
  validates_numericality_of :amount, greater_than: 0.0
  scope :transfers, -> { where.not(source_wallet_id: nil, target_wallet_id: nil) }
  scope :deposits, -> { where.not(source_wallet_id: nil).where(target_wallet_id: nil) }
  scope :withdraws, -> { where.not(target_wallet_id: nil).where(source_wallet_id: nil) }

  def act_type
    return 'transfer' if source_wallet_id.present? and target_wallet_id.present?
    return 'deposit' if source_wallet_id.present? and target_wallet_id.nil?

    'withdraw'
  end

  def source_wallet
    Wallet.find(source_wallet_id)
  rescue
    nil
  end

  def target_wallet
    Wallet.find(target_wallet_id)
  rescue
    nil
  end

end

# == Schema Information
#
# Table name: transactions
#
#  id               :bigint           not null, primary key
#  amount           :decimal(9, 2)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  source_wallet_id :integer
#  target_wallet_id :integer
#
