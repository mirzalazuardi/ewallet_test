FactoryBot.define do
  factory :entity do
    type { %w[User Team Stock].sample }
    name { { 'User' => Faker::Name, 'Team' => Faker::Team, 'Stock' => Faker::Company }[type].name }
  end
end

# == Schema Information
#
# Table name: entities
#
#  id         :bigint           not null, primary key
#  name       :string
#  type       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
