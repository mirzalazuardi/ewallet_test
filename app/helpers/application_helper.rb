module ApplicationHelper
  def fetch_entity_name(arg, type: nil)
    return if arg.nil?
    return arg.entity.name if arg.instance_of?(Wallet)
    return Wallet.find(arg).entity.name if type == :wallet_id

    Entity.find(arg).name
  end
end
