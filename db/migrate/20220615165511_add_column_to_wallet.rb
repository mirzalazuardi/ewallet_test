class AddColumnToWallet < ActiveRecord::Migration[7.0]
  def change
    add_column :wallets, :uuid, :string, null: false
    add_index :wallets, :uuid, unique: true
  end
end
