class Wallet < ApplicationRecord
  belongs_to :entity, dependent: :destroy
  has_many :credits, class_name: 'Transaction', foreign_key: 'source_wallet_id'
  has_many :debits, class_name: 'Transaction', foreign_key: 'target_wallet_id'

  validates :entity_id, presence: true
  validates :uuid, uniqueness: true

  before_create :assign_uuid

  def balance
    credits.sum(:amount) - debits.sum(:amount)
  end

  private

  def assign_uuid
    self.uuid = SecureRandom.uuid.gsub('-', '')
  end
end

# == Schema Information
#
# Table name: wallets
#
#  id         :bigint           not null, primary key
#  uuid       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  entity_id  :bigint           not null
#
# Indexes
#
#  index_wallets_on_entity_id  (entity_id)
#  index_wallets_on_uuid       (uuid) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (entity_id => entities.id)
#
