class EntitiesController < ApplicationController
  before_action :set_entity, only: %i[show edit update destroy]

  respond_to :html

  def index
    @entities = Entity.all
    respond_with(@entities)
  end

  def show
    respond_with(@entity)
  end

  def new
    @entity = Entity.new(entity_params)
    respond_with(@entity)
  end

  def edit; end

  def create
    @entity = Entity.new(entity_params)
    if @entity.save
      respond_to do |format|
        format.html { redirect_to entities_path, notice: "Quote was successfully created." }
        format.turbo_stream
      end
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    @entity.assign_attributes(entity_params)
    if @entity.save
      respond_to do |format|
        format.html { redirect_to entities_path, notice: "Quote was successfully updated." }
        format.turbo_stream
      end
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    @entity.destroy
    respond_to do |format|
      format.turbo_stream { render turbo_stream: turbo_stream.remove(@entity) }
      format.html         { redirect_to entities_url }
    end
  end

  private

  def set_entity
    @entity = Entity.find(params[:id])
  end

  def entity_params
    params.fetch(:entity, {}).permit(:name, :type)
  end
end
