Rails.application.routes.draw do
  resources :entities do
    get 'deposit', to: 'transactions#deposit'
    get 'withdraw', to: 'transactions#withdraw'
    get 'transfer', to: 'transactions#transfer'
    post 'compute', to: 'transactions#compute', on: :collection
  end
  resources :transactions, only: %w[index]
  root 'entities#index'
end
