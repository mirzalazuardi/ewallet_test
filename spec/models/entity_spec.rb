require 'rails_helper'

RSpec.describe Entity, type: :model do
  context 'validations' do
    describe 'presence' do
      it { should validate_presence_of(:name) }
      it { should validate_presence_of(:type) }
    end

    describe 'inclusion' do
      it { should validate_inclusion_of(:type).in_array(%w[User Team Stock]) }
    end
  end

  context 'callbacks' do
    describe 'after_save' do
      it { is_expected.to callback(:create_wallet).after(:create) }
    end
  end
end

# == Schema Information
#
# Table name: entities
#
#  id         :bigint           not null, primary key
#  name       :string
#  type       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
