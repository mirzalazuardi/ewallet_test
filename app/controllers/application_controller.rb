class ApplicationController < ActionController::Base
  include ActionView::RecordIdentifier

  def wallet(id = nil)
    return Entity.find(params[:entity_id]).wallet if params[:entity_id].present?
    Wallet.find(id)
  rescue
    nil
  end
end
