require 'rails_helper'

RSpec.feature "Entities", type: :feature do
  scenario "Create user" do
    visit root_path

    click_on "New entity"
    fill_in "entity_name", with: 'Mirzalazuardi'
    select 'User', from: 'entity_type'
    click_button "Submit"

    expect(page).to have_text("Mirzalazuardi").and have_text("User")
  end

  scenario "Create team" do
    visit root_path

    click_on "New entity"
    fill_in "entity_name", with: 'Alpha'
    select 'Team', from: 'entity_type'
    click_button "Submit"


    expect(page).to have_text("Alpha").and have_text("Team")
  end

  scenario "Create stock" do
    visit root_path

    click_on "New entity"
    fill_in "entity_name", with: 'APPL'
    select 'Stock', from: 'entity_type'
    click_button "Submit"


    expect(page).to have_text("APPL").and have_text("Stock")
  end

  scenario "Show user" do
    create(:entity, name: 'Mirzalazuardi', type: 'User')
    visit root_path

    click_on 'Show'

    expect(page).to have_text("Mirzalazuardi")
  end

  scenario "Show team" do
    create(:entity, name: 'Alpha', type: 'Team')
    visit root_path

    click_on 'Show'

    expect(page).to have_text("Alpha")
  end

  scenario "Show stock" do
    create(:entity, name: 'APPL', type: 'Team')
    visit root_path

    click_on 'Show'

    expect(page).to have_text("APPL")
  end

  scenario "Update user" do
    create(:entity, name: 'Mirzalazuardi', type: 'User')
    visit root_path

    click_on 'Edit'
    fill_in "entity_name", with: 'Hermawan'
    click_button "Submit"

    expect(page).to have_text("Hermawan")
  end

  scenario "Update team" do
    create(:entity, name: 'Alpha', type: 'Team')
    visit root_path

    click_on 'Edit'
    fill_in "entity_name", with: 'Bravo'
    click_button "Submit"

    expect(page).to have_text("Bravo")
  end

  scenario "Update stock" do
    create(:entity, name: 'APPL', type: 'Stock')
    visit root_path

    click_on 'Edit'
    fill_in "entity_name", with: 'GOOG'
    click_button "Submit"

    expect(page).to have_text("GOOG")
  end
end
