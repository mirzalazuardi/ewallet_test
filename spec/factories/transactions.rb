FactoryBot.define do
  factory :transaction do
    amount { "#{Faker::Number.decimal(l_digits: 2)}" }
  end
end

# == Schema Information
#
# Table name: transactions
#
#  id               :bigint           not null, primary key
#  amount           :decimal(9, 2)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  source_wallet_id :integer
#  target_wallet_id :integer
#
